﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raise.Monitor.Model;
using Raise.Monitor.Tools;

namespace Raise.Monitor.Services {
    public class MySqlStrategy : Strategy {
        public override List<KeyValueItem> GetKeyValueItems(string name) {
            throw new NotImplementedException();
        }

        public override PageData<MonitorLogRecordConfigView> GetConfigRulesWithLogs(int pageIndex, string keyWords, string method, string serviceName, int? status,
            DateTime beginTime, DateTime endTime) {
            throw new NotImplementedException();
        }

        public override PageData<SystemLog> GetSystemLogs(int pageIndex, string keyWords, string callsite, string logLevel, DateTime beginTime,
            DateTime endTime) {
            throw new NotImplementedException();
        }

        public override PageData<RuleConfig> GetConfigRules(int pageIndex, string keyWords, string requestType, string serviceName, int? runStatus,
            int? status) {
            throw new NotImplementedException();
        }

        public override void SetRuleConfigStatus(int id, int status) {
            throw new NotImplementedException();
        }

        public override void SetRuleConfigRunStatus(int id, int runStatus) {
            throw new NotImplementedException();
        }

        public override MessageInformation InsertRule(RuleConfig config) {
            throw new NotImplementedException();
        }

        public override MessageInformation SaveChanges(RuleConfig config) {
            throw new NotImplementedException();
        }
    }
}
